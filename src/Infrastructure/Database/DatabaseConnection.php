<?php
declare(strict_types=1);


namespace App\Infrastructure\Database;


use mysqli;
use mysqli_stmt;

/**
 * IMPORTANT!
 * Task list has not included or described DBAL. This one of copy paste classes with a little updates
 * PDO could be used too https://bitbucket.org/BonBonSlick/san-task/src/master/db_connect.php
 * With aka Service Container https://bitbucket.org/BonBonSlick/php-students-blog/src/master/includes/config.php
 *
 * @todo - Dig and create own simple DBAL with PDO
 * https://gist.github.com/bradtraversy/a77931605ba9b7cf3326644e75530464
 * https://github.com/nikiedev/simple-php-pdo-database-class/blob/master/lib/Db.class.php
 * https://github.com/lincanbin/PHP-PDO-MySQL-Class/blob/master/src/PDO.class.php
 * https://github.com/MiksZvirbulis/PHP-PDO-Class/blob/master/db.class.php
 * https://github.com/Abbe98/Simple-PDO/blob/master/src/pdo.php
 */
final class DatabaseConnection {
    public int                $query_count  = 0;
    private ?mysqli           $connection;
    private ?mysqli_stmt      $query;
    private bool              $show_errors  = true;
    private bool              $query_closed = true;

    public function __construct(
        ?string $host = null,
        ?string $user = null,
        ?string $pass = null,
        ?string $db = null,
        string $charset = 'utf8'
    ) {
        $host             = (string)($host ?? getenv('DB_HOST') ?? 'localhost');
        $db               = (string)($db ?? getenv('DB_DATABASE') ?? '');
        $user             = (string)($user ?? getenv('DB_USERNAME') ?? 'root');
        $pass             = (string)($pass ?? getenv('DB_PASSWORD') ?? '');
        $this->connection = new mysqli($host, $user, $pass, $db);
        if ($this->connection->connect_error) {
            $this->error('Failed to connect to MySQL - ' . $this->connection->connect_error);
        }
        $this->connection->set_charset($charset);
    }

    public function error($error): void {
        if (true === $this->show_errors) {
            exit($error);
        }
    }

    public function query(string $query): DatabaseConnection {
        if (false === $this->query_closed) {
            $this->query->close();
        }
        $statement   = $this->connection->prepare($query);
        $this->query = true === $statement instanceof mysqli_stmt ? $statement : null;
        if (null === $this->query) {
            $this->error(
                sprintf('Unable to prepare MySQL statement (check your syntax) ERROR - %s', $this->connection->error)
            );
        } else {
            $this->buildQuery();
        }

        return $this;
    }

    private function buildQuery(): void {
        if (1 < func_num_args()) {
            $args     = array_slice(func_get_args(), 1);
            $types    = '';
            $args_ref = [];
            foreach ($args as $k => &$arg) {
                if (true === is_array($args[$k])) {
                    foreach ($args[$k] as $j => &$a) {
                        $types      .= $this->_gettype($args[$k][$j]);
                        $args_ref[] = &$a;
                    }
                    continue;
                }
                $types      .= $this->_gettype($args[$k]);
                $args_ref[] = &$arg;
            }
            array_unshift($args_ref, $types);
            call_user_func_array([$this->query, 'bind_param'], $args_ref);
        }
        $this->query->execute();
        if ($this->query->errno) {
            $this->error('Unable to process MySQL query (check your params) - ' . $this->query->error);
        }
        $this->query_closed = false;
        $this->query_count++;
    }

    private function _gettype($var): string {
        $returnVal = 'b';
        if (true === is_string($var)) {
            $returnVal = 's';
        }
        if (true === is_float($var)) {
            $returnVal = 'd';
        }
        if (true === is_int($var)) {
            $returnVal = 'i';
        }
        return $returnVal;
    }

    public function all(callable $callback = null): array {
        $params = [];
        $row    = [];
        $meta   = $this->query->result_metadata();

        while ($field = $meta->fetch_field()) {
            $params[] = &$row[$field->name];
        }

        call_user_func_array([$this->query, 'bind_result'], $params);

        $result = [];
        while ($this->query->fetch()) {
            $r = [];
            foreach ($row as $key => $val) {
                $r[$key] = $val;
            }
            if (null !== $callback && true === is_callable($callback)) {
                $value = $callback($r);
                if ('break' === $value) {
                    break;
                }
            } else {
                $result[] = $r;
            }
        }

        $this->query->close();
        $this->query_closed = true;

        return $result;
    }

    public function single(): array {
        $params = [];
        $row    = [];
        $meta   = $this->query->result_metadata();
        while ($field = $meta->fetch_field()) {
            $params[] = &$row[$field->name];
        }
        call_user_func_array([$this->query, 'bind_result'], $params);
        $result = [];
        while ($this->query->fetch()) {
            foreach ($row as $key => $val) {
                $result[$key] = $val;
            }
        }
        $this->query->close();
        $this->query_closed = true;
        return $result;
    }

    public function close(): bool {
        return $this->connection->close();
    }

    public function totalRecords(): int {
        $this->query->store_result();
        return $this->query->num_rows;
    }

    public function affectedRows(): int {
        return $this->query->affected_rows;
    }

    public function lastInsertID() {
        return $this->connection->insert_id;
    }

}
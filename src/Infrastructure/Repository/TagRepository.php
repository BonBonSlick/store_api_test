<?php
declare(strict_types=1);


namespace App\Infrastructure\Repository;


use App\Domain\Entity\Core\AbsRepository;
use App\Domain\Entity\Core\PropertyDoesNotExist;
use App\Domain\Entity\Tag\ITagRepository;
use App\Domain\Entity\Tag\Tag;
use App\Domain\Entity\Tag\TagCollection;
use App\Domain\Entity\Tag\TagFilter;

final class TagRepository extends AbsRepository implements ITagRepository {

    /**
     * @throws PropertyDoesNotExist
     */
    public function many(TagFilter $filter): TagCollection {
        $this->query = 'SELECT * FROM `Tag`';
        $this->filter($filter);
        $this->query .= ';';

        return new TagCollection($this->serialize($this->db->query($this->query)->all(), Tag::class));
    }


    /**
     * @param TagFilter $filter
     */
    protected function filter($filter): void {
        if (true === $filter->isMostlySold()) {
            $selectFrom     = 'SELECT T.tag_name, SUM(P.price) as total_profit FROM Tag T';
            $joinTagConnect = 'JOIN TagConnect TC on T.id = TC.tag_id';
            $joinProduct    = 'JOIN Product P on TC.product_id = P.id';
            $groupBy        = 'GROUP BY T.id';
            $orderBy        = 'ORDER BY total_profit DESC';
            $joins          = sprintf('%s %s', $joinTagConnect, $joinProduct);
            $this->query    = sprintf('%s %s %s %s', $selectFrom, $joins, $groupBy, $orderBy);
        }
    }
}
<?php
declare(strict_types=1);


namespace App\Infrastructure\Repository;


use App\Domain\Entity\Core\AbsRepository;
use App\Domain\Entity\Core\PropertyDoesNotExist;
use App\Domain\Entity\User\IUserRepository;
use App\Domain\Entity\User\User;
use App\Domain\Entity\User\UserCollection;
use App\Domain\Entity\User\UserFilter;

final class UserRepository extends AbsRepository implements IUserRepository {

    /**
     * @throws PropertyDoesNotExist
     */
    public function many(UserFilter $filter): UserCollection {
        $this->query = 'SELECT * FROM `User` U';
        $this->filter($filter);
        $this->query .= ';';

        return new UserCollection($this->serialize($this->db->query($this->query)->all(), User::class));
    }


    /**
     * @param UserFilter $filter
     */
    protected function filter($filter): void {
        if (null !== $filter->getNeverBoughtFromStoreID()) {
            $selectFrom  = 'SELECT U.name, U.id as customer_id FROM User U';
            $subQuery    = sprintf(
                'SELECT O.customer_id FROM Product P JOIN OrderItem I on P.id = I.product_id JOIN `Order` O on I.order_id = O.id WHERE P.store_id = %d',
                $filter->getNeverBoughtFromStoreID()
            );
            $where       = sprintf('WHERE U.id NOT IN (%s)', $subQuery);
            $this->query = sprintf('%s %s', $selectFrom, $where);
        }
        if (true === $filter->getCalcTotalSpent()) {
            $selectFrom    = 'SELECT U.name, SUM(P.price) as total_spent FROM User U';
            $joinOrder     = 'JOIN `Order` O on O.customer_id = U.id';
            $joinOrderItem = 'JOIN OrderItem OI on O.id = OI.order_id';
            $joinProduct   = 'JOIN Product P on OI.product_id = P.id';
            $joins         = sprintf('%s %s %s', $joinOrder, $joinOrderItem, $joinProduct);
            $groupBy       = 'GROUP BY U.id';
            $having        = 'HAVING total_spent >= 1000';
            $this->query   = sprintf('%s %s %s %s', $selectFrom, $joins, $groupBy, $having);
        }
    }
}
<?php
declare(strict_types=1);


namespace App\Infrastructure\Repository;


use App\Domain\Entity\Core\AbsRepository;
use App\Domain\Entity\Core\PropertyDoesNotExist;
use App\Domain\Entity\Store\IStoreRepository;
use App\Domain\Entity\Store\Store;
use App\Domain\Entity\Store\StoreCollection;
use App\Domain\Entity\Store\StoreFilter;

final class StoreRepository extends AbsRepository implements IStoreRepository {

    /**
     * @throws PropertyDoesNotExist
     */
    public function many(StoreFilter $filter): StoreCollection {
        $this->query = 'SELECT * FROM Store S ';
        $this->filter($filter);
        $this->query .= ';';

        return new StoreCollection($this->serialize($this->db->query($this->query)->all(), Store::class));
    }


    /**
     * @param StoreFilter $filter
     */
    protected function filter($filter): void {
        if (0 < count($filter->getTagNames())) {
            $select        = 'SELECT S.name FROM Store S';
            $joinProducts  = 'JOIN Product P ON S.id = P.store_id';
            $joinTagsPivot = 'JOIN TagConnect TC ON P.id = TC.product_id';
            $joinTag       = 'JOIN Tag T ON TC.tag_id = T.id';
            $joins         = sprintf('%s %s %s', $joinProducts, $joinTagsPivot, $joinTag);
            $whereIn       = 'WHERE tag_name IN (';
            $countedArr    = count($filter->getTagNames()) - 1;
            foreach ($filter->getTagNames() as $index => $tagName) {
                if ($countedArr === $index) {
                    $whereIn .= sprintf('\'%s\'', $tagName);
                    continue;
                }
                $whereIn .= sprintf('\'%s\',', $tagName);
            }
            $whereIn     .= ')';
            $groupBy     = 'GROUP BY S.id';
            $this->query = sprintf('%s %s %s %s', $select, $joins, $whereIn, $groupBy);
        }
        if (true === $filter->hasNoSells()) {
            $selectFrom    = 'SELECT DISTINCT S.name FROM Store S';
            $where         = 'WHERE OI.id IS NULL';
            $joinProduct   = 'JOIN Product P ON P.store_id = S.id';
            $joinOrderItem = 'LEFT JOIN OrderItem OI ON P.id = OI.product_id';
            $joins         = sprintf('%s %s', $joinProduct, $joinOrderItem);
            $this->query   = sprintf('%s %s %s', $selectFrom, $joins, $where);
        }
        if (null !== $filter->getShowStoreMonthly()) {
            $selectFrom
                           = 'SELECT S.name, YEAR(O.order_date)  AS sales_y,  MONTH(O.order_date) AS sales_m, SUM(P.price) AS total_profit, COUNT(P.id) AS total_products_sold FROM Store S';
            $joinProduct   = 'JOIN Product P ON P.store_id = S.id';
            $joinOrderItem = 'JOIN OrderItem OI ON P.id = OI.product_id';
            $joinOrder     = 'JOIN `Order` O on O.id = OI.order_id';
            $joins         = sprintf('%s %s %s', $joinProduct, $joinOrderItem, $joinOrder);
            $groupBy       = 'GROUP BY sales_y,sales_m';
            $orderBy       = 'ORDER BY sales_y,sales_m, total_profit';
            $this->query   = sprintf('%s %s %s %s', $selectFrom, $joins, $groupBy, $orderBy);
        }
    }

    public function calculateStoreEarningsNew(StoreFilter $filter): string {
        $selectFrom    = 'SELECT SUM(P.price) AS total_profit FROM Store S';
        $joinProduct   = 'JOIN Product P on S.id = P.store_id';
        $joinOrderItem = 'JOIN OrderItem OI on P.id = OI.product_id';
        $where         = 'WHERE S.id = :storeId';
        $groupBy       = 'GROUP BY S.id';
        $joins         = sprintf('%s %s', $joinProduct, $joinOrderItem);
        $this->query   = sprintf('%s %s %s %s', $selectFrom, $joins, $where, $groupBy);
//        dd($this->query);
        return (string)$this->dbManager->fetch($this->query, ['storeId' => $filter->id])['total_profit'];
    }
}
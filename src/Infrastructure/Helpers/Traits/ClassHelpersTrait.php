<?php
declare(strict_types=1);
/**
 * (c) BonBonSlick
 */

namespace App\Infrastructure\Helpers\Traits;

use ReflectionClass;
use ReflectionException;
use Verraes\ClassFunctions\ClassFunctions;

trait ClassHelpersTrait {
    /**
     * @throws ReflectionException
     */
    public function areTheObjectsTheSame(object $object, object $compareWith): bool {
        $reflection = new ReflectionClass($object);
        foreach ($reflection->getProperties() as $property) {
            $name = $property->getName();
            if ($object->$name !== $compareWith->$name) {
                return false;
            }
        }

        return true;
    }
}

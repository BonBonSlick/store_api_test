<?php
declare(strict_types=1);
/**
 * (c) BonBonSlick
 */
/*
 * Created by BonBonSlick
 * Contacts: google it
 * Date: 10/6/18
 * Time: 7:21 PM
 */

namespace App\Infrastructure\Helpers\Traits;


use App\Domain\Entity\Core\PropertyDoesNotExist;

trait SerializerTrait {

    /**
     * @throws PropertyDoesNotExist
     */
    public function serialize(array $entities, string $toObject): array {
        $stack = [];
        foreach ($entities as $item) {
            $object = new $toObject();
            foreach ($item as $propertyName => $propValue) {
                if (false === property_exists($object, $propertyName)) {
                    continue;
                }
                $object->$propertyName = $propValue;
            }
            $stack[] = $object;
        }

        return $stack;
    }
}

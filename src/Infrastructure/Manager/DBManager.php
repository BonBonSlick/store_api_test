<?php
declare(strict_types=1);


namespace App\Infrastructure\Manager;

use PDO;
use PDOException;
use RuntimeException;

final class DBManager {
    public bool  $connected  = false;
    public bool  $errors     = true;
    private ?PDO $connection = null;

    /**
     * @throws RuntimeException
     */
    public function __construct(
        ?string $host = null,
        ?string $user = null,
        ?string $pass = null,
        ?string $db = null,
        ?string $port = '3306',
        ?string $charset = 'utf8'
    ) {
        $host    = (string)($host ?? getenv('DB_HOST') ?? 'localhost');
        $db      = (string)($db ?? getenv('DB_DATABASE') ?? '');
        $user    = (string)($user ?? getenv('DB_USERNAME') ?? 'root');
        $pass    = (string)($pass ?? getenv('DB_PASSWORD') ?? '');
        $dsn     = sprintf('mysql:host=%s;dbname=%s', $host, $db);
        $options = [
            PDO::ATTR_PERSISTENT         => true,
            PDO::ATTR_EMULATE_PREPARES   => false,
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ];
        try {
            $this->connected  = true;
            $this->connection = new PDO ($dsn, $user, $pass, $options);
        } catch (PDOException $e) {
            $this->connected = false;
            if ($this->errors === true) {
                throw new RuntimeException($e->getMessage());
            }
        }
    }

    public function __destruct() {
        $this->connected  = false;
        $this->connection = null;
    }

    public function fetch(string $query, array $parameters = []) {
        if (false === $this->connected) {
            return false;
        }
        try {
            $query = $this->connection->prepare($query);
            $query->execute($parameters);
            return $query->fetch();
        } catch (PDOException $e) {
            if ($this->errors === true) {
                throw new RuntimeException($e->getMessage());
            }
        }
        return false;
    }

    public function fetchAll(string $query, array $parameters = []) {
        if (false === $this->connected) {
            return false;
        }
        try {
            $query = $this->connection->prepare($query);
//            dd($query);
            $query->execute($parameters);
            return $query->fetchAll();
        } catch (PDOException $e) {
            if ($this->errors === true) {
                throw new RuntimeException($e->getMessage());
            }
        }
        return false;
    }

    public function update($query, $parameters = []): bool {
        if (false === $this->connected) {
            return false;
        }
        return $this->insert($query, $parameters);
    }

    public function insert($query, $parameters = []): ?bool {
        if (false === $this->connected) {
            return false;
        }
        try {
            $query = $this->connection->prepare($query);
            $query->execute($parameters);
            return true;
        } catch (PDOException $e) {
            if ($this->errors === true) {
                throw new RuntimeException($e->getMessage());
            }
            return false;
        }
    }

    public function delete($query, $parameters = []): bool {
        if (false === $this->connected) {
            return false;
        }
        return $this->delete($query, $parameters);
    }

    public function tableExists(string $table): ?bool {
        if (false === $this->connected) {
            return false;
        }
        try {
            $query = $this->count("SHOW TABLES LIKE '$table'");
            return 0 < $query;
        } catch (PDOException $e) {
            if ($this->errors === true) {
                throw new RuntimeException($e->getMessage());
            }
            return false;
        }
    }

    public function count($query, $parameters = []) {
        if (false === $this->connected) {
            return false;
        }
        try {
            $query = $this->connection->prepare($query);
            $query->execute($parameters);
            return $query->rowCount();
        } catch (PDOException $e) {
            if ($this->errors === true) {
                throw new RuntimeException($e->getMessage());
            }
            return false;
        }
    }
}
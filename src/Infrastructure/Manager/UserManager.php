<?php
declare(strict_types=1);


namespace App\Infrastructure\Manager;


use App\Domain\Entity\User\Administrator;
use App\Domain\Entity\User\Customer;
use App\Domain\Entity\User\IUserManager;
use App\Domain\Entity\User\Seller;
use App\Domain\Entity\User\User;
use App\Domain\Entity\User\UserCollection;
use App\Infrastructure\Helpers\Traits\ClassHelpersTrait;
use ReflectionException;

final class UserManager implements IUserManager {
    use ClassHelpersTrait;

    private UserCollection $inMemoryUsers;

    public function __construct() {
        $this->inMemoryUsers = new UserCollection(
            [
                new Customer(1, 'John', '100.55', '34'),
                new Seller(2, 'Alex', '550.87', '781'),
                new Administrator(3, 'Arnold', '{reports, sales, users}'),
            ]
        );
    }

    /**
     * @throws ReflectionException
     */
    public function getUserInfo(User $user): ?User {
        foreach ($this->getInMemoryUsers()->getItems() as $inMemoryUser) {
            if (false === $this->areTheObjectsTheSame($inMemoryUser, $user)) {
                continue;
            }
            return $user;
        }

        return null;
    }

    public function getInMemoryUsers(): UserCollection {
        return $this->inMemoryUsers;
    }
}
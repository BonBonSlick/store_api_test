<?php
declare(strict_types=1);


namespace App\Infrastructure\Manager;


use App\Domain\Entity\Store\StoreFilter;
use App\Infrastructure\Repository\StoreRepository;

final class StoreManager {
    public DBManager       $dbManager;
    public StoreRepository $storeRepository;

    public function __construct(DBManager $dbManager) {
        $this->dbManager       = $dbManager;
        $this->storeRepository = new StoreRepository();
    }

    public function calculateStoreEarningsOldFixed(int $storeId): string {
        $totalAmount = 0;
        $tagCount    = $this->getTotalUniqueTags();

        foreach ($this->getProducts($storeId) as $product) {
            $productId = $product['id'];

            foreach ($this->getOrderItems($productId) as $item) {
                if ($productId !== $item['product_id']) {
                    continue;
                }
                $totalAmount += $product['price'];

            }

            $tags        = $this->getProductTags($productId);
            $totalAmount *= (1 + count($tags) / $tagCount);

            foreach ($tags as $tag) {
                if ('Christmas' === $tag['tag_name']) {
                    $totalAmount += 0.1;
                }
                if ('Free' === $tag['tag_name']) {
                    $totalAmount /= 2;
                }
            }
        }

        return (string)number_format((float)$totalAmount, 2, '.', '');
    }

    public function getProducts(int $storeId): array {
        $query = 'SELECT * FROM Product P WHERE P.store_id = :store';
        return $this->dbManager->fetchAll($query, ['store' => $storeId]);
    }

    public function getOrderItems(int $productId): array {
        $query = 'SELECT * FROM OrderItem OI WHERE OI.product_id = :product';
        return $this->dbManager->fetchAll($query, ['product' => $productId]);
    }

    public function getProductTags(int $productId): array {
        $query = 'SELECT * FROM Tag T WHERE T.id IN(SELECT tag_id FROM TagConnect WHERE product_id = :product)';
        return $this->dbManager->fetchAll($query, ['product' => $productId]);
    }

    public function getTotalUniqueTags(): int {
        return $this->dbManager->count('SELECT * FROM Tag T', []);
    }

    public function calculateStoreEarningsNew(StoreFilter $filter): string {
        return $this->storeRepository->calculateStoreEarningsNew($filter);
    }
}
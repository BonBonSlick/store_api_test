SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE `Order`
(
    `id`           int(11)     NOT NULL,
    `customer_id`  int(11)     NOT NULL,
    `order_number` varchar(20) NOT NULL,
    `order_date`   datetime    NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO `Order` (`id`, `customer_id`, `order_number`, `order_date`)
VALUES (1, 1, 'Order 1', '2019-01-01 00:00:00'),
       (2, 2, 'Order 2', '2019-01-15 00:00:00'),
       (3, 3, 'Order 3', '2019-01-30 00:00:00'),
       (4, 1, 'Order 4', '2019-04-03 00:00:00'),
       (5, 2, 'Order 5', '2019-04-11 00:00:00'),
       (6, 1, 'Order 6', '2019-07-01 00:00:00'),
       (7, 5, 'Order 7', '2019-08-09 00:00:00'),
       (8, 3, 'Order 8', '2019-07-05 00:00:00'),
       (9, 1, 'Order 9', '2019-08-05 00:00:00'),
       (10, 2, 'Order 10', '2019-02-14 00:00:00');

CREATE TABLE `OrderItem`
(
    `id`         int(11) NOT NULL,
    `order_id`   int(11) NOT NULL,
    `product_id` int(11) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO `OrderItem` (`id`, `order_id`, `product_id`)
VALUES (1, 1, 1),
       (2, 1, 2),
       (3, 1, 3),
       (4, 1, 4),
       (5, 1, 5),
       (6, 1, 6),
       (7, 1, 7),
       (8, 1, 8),
       (9, 1, 9),
       (10, 1, 10),
       (11, 2, 11),
       (12, 2, 12),
       (13, 2, 13),
       (14, 2, 14),
       (15, 2, 15),
       (16, 2, 18),
       (17, 2, 19),
       (18, 2, 20),
       (19, 3, 5),
       (20, 3, 6),
       (21, 3, 13),
       (22, 3, 14),
       (23, 3, 15),
       (24, 4, 7),
       (25, 4, 12),
       (26, 4, 4),
       (27, 4, 14),
       (28, 5, 6),
       (29, 5, 7),
       (30, 6, 5),
       (31, 7, 8),
       (32, 7, 12),
       (33, 8, 1),
       (34, 9, 14),
       (35, 9, 15),
       (36, 9, 18),
       (37, 10, 1),
       (38, 10, 4);

CREATE TABLE `Product`
(
    `id`       int(11)        NOT NULL,
    `store_id` int(11)        NOT NULL,
    `name`     varchar(50)    NOT NULL,
    `price`    decimal(10, 0) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO `Product` (`id`, `store_id`, `name`, `price`)
VALUES (1, 1, 'Product 1', '200'),
       (2, 1, 'Product 2', '340'),
       (3, 1, 'Product 3', '135'),
       (4, 1, 'Product 4', '200'),
       (5, 1, 'Product 5', '48'),
       (6, 2, 'Product 6', '39'),
       (7, 2, 'Product 7', '289'),
       (8, 2, 'Product 8', '100'),
       (9, 2, 'Product 9', '120'),
       (10, 2, 'Product 10', '230'),
       (11, 2, 'Product 11', '60'),
       (12, 2, 'Product 12', '54'),
       (13, 2, 'Product 13', '37'),
       (14, 2, 'Product 14', '80'),
       (15, 2, 'Product 15', '20'),
       (16, 4, 'Product 16', '50'),
       (17, 4, 'Product 17', '28'),
       (18, 5, 'Product 18', '60'),
       (19, 5, 'Product 19', '110'),
       (20, 5, 'Product 20', '230');

CREATE TABLE `Store`
(
    `id`   int(11)     NOT NULL,
    `name` varchar(50) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO `Store` (`id`, `name`)
VALUES (1, 'Store 1'),
       (2, 'Store 2'),
       (3, 'Store 3'),
       (4, 'Store 4'),
       (5, 'Store 5');

CREATE TABLE `Tag`
(
    `id`       int(11)     NOT NULL,
    `tag_name` varchar(50) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO `Tag` (`id`, `tag_name`)
VALUES (1, 'Christmas'),
       (2, 'Winter'),
       (3, 'Weather'),
       (4, 'Summer'),
       (5, 'Spring');

CREATE TABLE `TagConnect`
(
    `id`         int(11) NOT NULL,
    `tag_id`     int(11) NOT NULL,
    `product_id` int(11) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO `TagConnect` (`id`, `tag_id`, `product_id`)
VALUES (42, 1, 1),
       (43, 2, 1),
       (44, 3, 1),
       (45, 4, 1),
       (46, 5, 1),
       (47, 3, 2),
       (48, 4, 2),
       (49, 5, 2),
       (50, 2, 3),
       (51, 5, 3),
       (52, 1, 4),
       (53, 5, 4),
       (54, 3, 5),
       (55, 4, 5),
       (56, 5, 5),
       (57, 1, 6),
       (58, 2, 6),
       (59, 1, 7),
       (60, 4, 7),
       (61, 5, 7),
       (62, 2, 8),
       (63, 1, 9),
       (64, 4, 9),
       (65, 5, 9),
       (66, 5, 10),
       (67, 1, 11),
       (68, 2, 11),
       (69, 1, 12),
       (70, 3, 12),
       (71, 5, 13),
       (72, 1, 14),
       (73, 2, 14),
       (74, 1, 16),
       (75, 5, 16),
       (76, 3, 17),
       (77, 4, 17),
       (78, 5, 17),
       (79, 4, 18),
       (80, 1, 19),
       (81, 1, 20),
       (82, 2, 20);

CREATE TABLE `User`
(
    `id`   int(11)     NOT NULL,
    `name` varchar(50) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO `User` (`id`, `name`)
VALUES (1, 'User 1'),
       (2, 'User 2'),
       (3, 'User 3'),
       (4, 'User 4'),
       (5, 'User 5');


ALTER TABLE `Order`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `OrderItem`
    ADD UNIQUE KEY `id` (`id`);

ALTER TABLE `Product`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `Store`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `Tag`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `TagConnect`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `User`
    ADD PRIMARY KEY (`id`);


ALTER TABLE `Order`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 11;

ALTER TABLE `OrderItem`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 39;

ALTER TABLE `Product`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 21;

ALTER TABLE `Store`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 6;

ALTER TABLE `Tag`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 6;

ALTER TABLE `TagConnect`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 83;

ALTER TABLE `User`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;

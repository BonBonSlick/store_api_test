<?php
declare(strict_types=1);


namespace App\Domain\Entity\User;


final class Seller extends User {
    public string    $product_count;
    public string    $earnings_balance;

    public function __construct(int $id, string $name, string $product_count, string $earnings_balance) {
        parent::__construct($id, $name);
        $this->product_count    = $product_count;
        $this->earnings_balance = $earnings_balance;
    }
}
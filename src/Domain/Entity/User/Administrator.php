<?php
declare(strict_types=1);


namespace App\Domain\Entity\User;


final class Administrator extends User {
    public string    $permissions;

    public function __construct(int $id, string $name, string $permissions) {
        parent::__construct($id, $name);
    }
}
<?php
declare(strict_types=1);


namespace App\Domain\Entity\User;


use App\Domain\Entity\Core\AbsArrayCollection;
use App\Domain\Entity\Core\BadParamType;

final class UserCollection extends AbsArrayCollection {

    public function __construct(array $items = []) {
        foreach ($items as $item) {
            $this->addItem($item);
        }
    }

    /**
     * @throws BadParamType
     */
    public function addItem($item): void {
        if (false === $item instanceof User) {
            throw new BadParamType();
        }
        $this->items[] = $item;
    }
}
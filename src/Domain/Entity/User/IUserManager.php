<?php
declare(strict_types=1);


namespace App\Domain\Entity\User;

interface IUserManager {
    public function getUserInfo(User $user);
}
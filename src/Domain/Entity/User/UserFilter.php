<?php
declare(strict_types=1);


namespace App\Domain\Entity\User;


final class UserFilter {
    public ?int  $neverBoughtFromStoreID = null;
    public bool  $calcTotalSpent         = false;

    public function getNeverBoughtFromStoreID(): ?int {
        return $this->neverBoughtFromStoreID;
    }

    public function setNeverBoughtFromStoreID(?int $neverBoughtFromStoreID): self {
        $this->neverBoughtFromStoreID = $neverBoughtFromStoreID;
        return $this;
    }

    public function getCalcTotalSpent(): bool {
        return $this->calcTotalSpent;
    }

    public function setCalcTotalSpent(bool $calcTotalSpent): self {
        $this->calcTotalSpent = $calcTotalSpent;
        return $this;
    }

}
<?php
declare(strict_types=1);


namespace App\Domain\Entity\User;

interface IUserRepository {
    public function many(UserFilter $filter);
}
<?php
declare(strict_types=1);


namespace App\Domain\Entity\User;


final class Customer extends User {
    public string    $balance;
    public string    $purchase_count;

    public function __construct(int $id, string $name, string $balance, string $purchase_count) {
        parent::__construct($id, $name);
        $this->balance        = $balance;
        $this->purchase_count = $purchase_count;
    }
}
<?php
declare(strict_types=1);


namespace App\Domain\Entity\User;


class User {
    public ?string $name;
    public ?int    $id;

    public function __construct(?int $id = null, ?string $name = null) {
        $this->id   = $id;
        $this->name = $name;
    }
}
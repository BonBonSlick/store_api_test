<?php
declare(strict_types=1);


namespace App\Domain\Entity\Product;


use App\Domain\Entity\Core\AbsArrayCollection;

final class ProductCollection extends AbsArrayCollection {

    public function __construct(array $items = []) {
        foreach ($items as $item) {
            $this->addItem($item);
        }
    }

    public function addItem($item): void {
//        if(false === ($item instanceof Store)){
//            throw new BadParamType();
//        }
//        var_dump($item);
//        die;
    }
}
<?php
declare(strict_types=1);


namespace App\Domain\Entity\Tag;

interface ITagRepository {
    public function many(TagFilter $filter);
}
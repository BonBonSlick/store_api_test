<?php
declare(strict_types=1);


namespace App\Domain\Entity\Tag;


final class TagFilter {
    public bool $mostlySold = false;

    public function isMostlySold(): bool {
        return $this->mostlySold;
    }

    public function setMostlySold(bool $mostlySold): self {
        $this->mostlySold = $mostlySold;
        return $this;
    }

}
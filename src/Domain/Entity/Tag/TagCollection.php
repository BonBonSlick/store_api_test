<?php
declare(strict_types=1);


namespace App\Domain\Entity\Tag;


use App\Domain\Entity\Core\AbsArrayCollection;
use App\Domain\Entity\Core\BadParamType;

final class TagCollection extends AbsArrayCollection {

    public function __construct(array $items = []) {
        foreach ($items as $item) {
            $this->addItem($item);
        }
    }

    public function addItem($item): void {
        $got      = \get_class($item);
        $expected = Tag::class;
        if ($got !== $expected) {
            throw new BadParamType();
        }
        $this->items[] = $item;
    }
}
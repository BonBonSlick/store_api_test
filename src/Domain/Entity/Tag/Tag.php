<?php
declare(strict_types=1);


namespace App\Domain\Entity\Tag;


final class Tag {
    public string $tag_name;
    public string $total_profit;
}
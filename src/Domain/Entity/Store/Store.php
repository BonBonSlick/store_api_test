<?php
declare(strict_types=1);


namespace App\Domain\Entity\Store;


use App\Domain\Entity\Core\IAggregateRoot;
use App\Domain\Entity\Product\ProductCollection;

final class Store implements IAggregateRoot {
    public int               $id;
    public string            $name;
    public int               $sales_y;
    public int               $sales_m;
    public string            $total_profit;
    public int               $total_products_sold;
    public ProductCollection $products;

    public function __construct() {
//        $this->products = new ProductCollection();
    }
}
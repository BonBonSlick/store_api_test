<?php
declare(strict_types=1);


namespace App\Domain\Entity\Store;


final class StoreFilter {
    public ?int  $id;
    public array $tagNames         = [];
    public bool  $hasNoSells       = false;
    public ?int  $showStoreMonthly = null;

    public function getTagNames(): array {
        return $this->tagNames;
    }

    public function setTagNames(array $tagNames): self {
        $this->tagNames = $tagNames;
        return $this;
    }

    public function hasNoSells(): bool {
        return $this->hasNoSells;
    }

    public function setHasNoSells(bool $hasNoSells): self {
        $this->hasNoSells = $hasNoSells;
        return $this;
    }

    public function getShowStoreMonthly(): ?int {
        return $this->showStoreMonthly;
    }

    public function setShowStoreMonthly(?int $showStoreMonthly): self {
        $this->showStoreMonthly = $showStoreMonthly;
        return $this;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setId(?int $id): self {
        $this->id = $id;
        return $this;
    }

}
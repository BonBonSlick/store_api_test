<?php
declare(strict_types=1);


namespace App\Domain\Entity\Store;


use App\Domain\Entity\Core\AbsArrayCollection;
use App\Domain\Entity\Core\BadParamType;

final class StoreCollection extends AbsArrayCollection {

    /**
     * @throws BadParamType
     */
    public function __construct(array $items = []) {
        foreach ($items as $item) {
            $this->addItem($item);
        }
    }

    /**
     * @throws BadParamType
     */
    public function addItem($item): void {
        $got      = \get_class($item);
        $expected = Store::class;
        if ($got !== $expected) {
            throw new BadParamType();
        }
        $this->items[] = $item;
    }
}
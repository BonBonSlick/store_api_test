<?php
declare(strict_types=1);


namespace App\Domain\Entity\Store;


interface IStoreRepository {
    public function many(StoreFilter $filter): StoreCollection;

    public function calculateStoreEarningsNew(StoreFilter $filter): string;
}
<?php
declare(strict_types=1);


namespace App\Domain\Entity\Core;


use Exception;

final class BadParamType extends Exception {
    public $message = 'Wrong parameter type';
}
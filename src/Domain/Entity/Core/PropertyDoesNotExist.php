<?php
declare(strict_types=1);


namespace App\Domain\Entity\Core;


use Exception;

final class PropertyDoesNotExist extends Exception {
    public static function fromName(string $propertyName): self {
        return new self(\sprintf('Property "%s" does not exist', $propertyName));
    }
}
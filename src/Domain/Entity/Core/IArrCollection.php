<?php
declare(strict_types=1);


namespace App\Domain\Entity\Core;


interface  IArrCollection {
    public function addItem(IAggregateRoot $item): void;
}
<?php
declare(strict_types=1);


namespace App\Domain\Entity\Core;


abstract class AbsArrayCollection implements IArrCollection {
    protected array $items = [];

    public function first(): object {
        return $this->items[0];
    }

    public function last(): object {
        return $this->items[count($this->items) - 1];
    }

    public function toArray(): array {
        $stack = [];
        foreach ($this->getItems() as $item) {
            $stack[] = (array)$item;
        }
        return $stack;
    }

    public function getItems(): array {
        return $this->items;
    }

}
<?php
declare(strict_types=1);


namespace App\Domain\Entity\Core;


use App\Infrastructure\Database\DatabaseConnection;
use App\Infrastructure\Helpers\Traits\SerializerTrait;
use App\Infrastructure\Manager\DBManager;

abstract class AbsRepository {
    use SerializerTrait;

    protected DatabaseConnection $db;
    protected DBManager          $dbManager;
    protected ?string            $query;

    public function __construct() {
        $this->db        = new DatabaseConnection();
        $this->dbManager = new DBManager();
    }

    abstract protected function filter($filter): void;
}
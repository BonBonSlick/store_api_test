- ```server@user: mysql -u root -p```
- ```MariaDB > create database tests;```
- ```MariaDB > exit;```
- ```server@user:  mysql -u admin -p tests < /var/www/test_api_task/src/Infrastructure/Migration/db_dump.sql```

DB docs:
- ```$account = $db->query('SELECT * FROM accounts WHERE username = ? AND password = ?', 'test', 'test')->single();```

- ```$account = $db->query('SELECT * FROM accounts WHERE username = ? AND password = ?', array('test', 'test'))->single();```

- ```$accounts = $db->query('SELECT * FROM accounts')->all();```

- ```$db->query('SELECT * FROM accounts')->totalRecords();```

- ```$db->query('SELECT * FROM accounts')->fetchAll(function($account) { echo $account['name'];  });``` -

 You can specify a callback if you do not want the results being stored in an array (useful for large amounts of data)
  If you need to break the loop you can add: ```return 'break'; ```
- ```$db->query('INSERT INTO accounts (username,password,email,name) VALUES (?,?,?,?)', 'test', 'test', 'test@gmail.com', 'Test')->affectedRows();```

- ```$db->query_count;```

- ```$db->lastInsertID();```

- ```$db->close();```

Also there is easy to use PDO in DBManager::class

NOTEs:
- When running dump from mysql use ```mysqldump --compatible=postgresql ```
- would be nice to provide  package or class for DB layer. Otherwise have to write own for testing.
- would be nice to provide  more data for performance optimizations
- if php does not work on your machine, you can run native_sql_tests.txt after mariadb < dump.sql
<?php
declare(strict_types=1);
/**
 * (c) BonBonSlick
 */

namespace App\Tests\Unit;

use App\Domain\Entity\User\Customer;
use App\Infrastructure\Helpers\Traits\SerializerTrait;
use App\Infrastructure\Manager\UserManager;
use App\Tests\AbsTest;
use Exception;

final class UserManagerTest extends AbsTest {
    use SerializerTrait;

    /**
     * @test
     *
     * @throws Exception
     */
    public function neverBoughtProductFromStore5(): void {
        $manager  = new UserManager();
        $customer = new Customer(1, 'John', '100.55', '34');
        $expected = [
            "balance"        => "100.55",
            "purchase_count" => "34",
            "name"           => "John",
            "id"             => 1,
        ];
        $actual   = (array)$manager->getUserInfo($customer);
        self::assertSame(array_diff($expected, $actual), array_diff($actual, $expected));
    }
}

<?php
declare(strict_types=1);
/**
 * (c) BonBonSlick
 */

namespace App\Tests;

use Dotenv\Dotenv;
use PHPUnit\Framework\TestCase;

abstract class AbsTest extends TestCase {
    final protected function setUp(): void {
        $dotenv = Dotenv::createImmutable(sprintf('%s/../', __DIR__));
        $dotenv->load();
    }
}

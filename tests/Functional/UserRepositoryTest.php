<?php
declare(strict_types=1);
/**
 * (c) BonBonSlick
 */

namespace App\Tests\Functional;

use App\Domain\Entity\User\UserFilter;
use App\Infrastructure\Helpers\Traits\SerializerTrait;
use App\Infrastructure\Repository\UserRepository;
use App\Tests\AbsTest;
use Exception;

final class UserRepositoryTest extends AbsTest {
    use SerializerTrait;

    /**
     * @test
     *
     * @throws Exception
     */
    public function calculateStoreEarnings(): void {
        $repository = new UserRepository();
        $filter     = (new UserFilter())->setNeverBoughtFromStoreID(5);
        $expected   = [
            ['name' => 'User 3'],
            ['name' => 'User 4'],
            ['name' => 'User 5'],
        ];
        $actual     = $repository->many($filter)->toArray();
        self::assertSame(array_diff($expected, $actual), array_diff($actual, $expected));
    }

    /**
     * @test
     *
     * @throws Exception
     */
    public function spentMoreThan1000(): void {
        $repository = new UserRepository();
        $filter     = (new UserFilter())->setCalcTotalSpent(true);
        $expected   = [
            ['name' => 'User 1'],
            ['name' => 'User 2'],
        ];
        $actual     = $repository->many($filter)->toArray();
        self::assertSame(array_diff($expected, $actual), array_diff($actual, $expected));
    }

}

<?php
declare(strict_types=1);
/**
 * (c) BonBonSlick
 */

namespace App\Tests\Functional;

use App\Domain\Entity\Tag\TagFilter;
use App\Infrastructure\Helpers\Traits\SerializerTrait;
use App\Infrastructure\Repository\TagRepository;
use App\Tests\AbsTest;
use Exception;

final class TagRepositoryTest extends AbsTest {
    use SerializerTrait;

    /**
     * @test
     *
     * @throws Exception
     */
    public function mostProfitable(): void {
        $repository = new TagRepository();
        $filter     = (new TagFilter())->setMostlySold(true);
        $expected   = [
            [
                'tag_name'     => 'Spring',
                'total_profit' => "1677",
            ],
            [
                'tag_name'     => 'Christmas',
                'total_profit' => "1432",
            ],
            [
                'tag_name'     => 'Summer',
                'total_profit' => "1085",
            ],
            [
                'tag_name'     => 'Weather',
                'total_profit' => "670",
            ],
            [
                'tag_name'     => 'Winter',
                'total_profit' => "664",
            ],
        ];
        $actual     = $repository->many($filter)->toArray();
        self::assertSame(array_diff($expected, $actual), array_diff($actual, $expected));
    }
}

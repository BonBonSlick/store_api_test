<?php
declare(strict_types=1);
/**
 * (c) BonBonSlick
 */

namespace App\Tests\Functional;

use App\Domain\Entity\Store\StoreFilter;
use App\Infrastructure\Manager\DBManager;
use App\Infrastructure\Manager\StoreManager;
use App\Tests\AbsTest;
use Exception;

final class StoreManagerTest extends AbsTest {
    /**
     * @test
     *
     * @throws Exception
     */
    public function getProducts(): void {
        $manager  = new StoreManager(new DBManager());
        $expected = [
            [
                "id"       => 16,
                "store_id" => 4,
                "name"     => "Product 16",
                "price"    => "50",
            ],
            [
                "id"       => 17,
                "store_id" => 4,
                "name"     => "Product 17",
                "price"    => "28",
            ],
        ];
        $actual   = $manager->getProducts(4);
        self::assertSame(array_diff($expected, $actual), array_diff($actual, $expected));
    }

    /**
     * @test
     *
     * @throws Exception
     */
    public function getOrderItems(): void {
        $manager = new StoreManager(new DBManager());
//        dd($manager->getOrderItems(2));
        $expected = [
            [
                "id"         => 2,
                "order_id"   => 1,
                "product_id" => 2,
            ],
        ];
        $actual   = $manager->getOrderItems(2);
        self::assertSame(array_diff($expected, $actual), array_diff($actual, $expected));
    }

    /**
     * @test
     *
     * @throws Exception
     */
    public function getProductTags(): void {
        $manager = new StoreManager(new DBManager());
        //        $filter  = (new StoreFilter())->setId(3);
//        dd($manager->getProductTags(6));
        $expected = [
            [
                "id"       => 1,
                "tag_name" => "Christmas",
            ],
            [
                "id"       => 2,
                "tag_name" => "Winter",
            ],
        ];
        $actual   = $manager->getProductTags(6);
        self::assertSame(array_diff($expected, $actual), array_diff($actual, $expected));
    }

    /**
     * @test
     *
     * @throws Exception
     */
    public function getTotalUniqueTags(): void {
        $manager = new StoreManager(new DBManager());
        self::assertSame(5, $manager->getTotalUniqueTags());
    }

    /**
     * @test
     *
     * @throws Exception
     */
    public function calculateStoreEarningsOldFixed(): void {
//        $eventName = 'calculateStoreEarnings';
//        $stopwatch = new Stopwatch();
//        $stopwatch->start($eventName);
        self::assertSame('641.98', (new StoreManager(new DBManager()))->calculateStoreEarningsOldFixed(5));
        //  4.00 MiB - 1 ms up to 5 ms
//        dd((string)$stopwatch->stop($eventName));
    }

    /**
     * @test
     *
     * @throws Exception
     */
    public function calculateStoreEarningsNew(): void {
//        $eventName = 'calculateStoreEarnings';
//        $stopwatch = new Stopwatch();
//        $stopwatch->start($eventName);
        $manager = new StoreManager(new DBManager());
        $filter  = (new StoreFilter())->setId(5);
        self::assertSame('460', $manager->calculateStoreEarningsNew($filter));
        //  4.00 MiB - 1 ms up to 2 ms
//        dd((string)$stopwatch->stop($eventName));
    }

}

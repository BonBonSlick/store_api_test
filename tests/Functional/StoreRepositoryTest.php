<?php
declare(strict_types=1);
/**
 * (c) BonBonSlick
 */

namespace App\Tests\Functional;

use App\Domain\Entity\Store\StoreFilter;
use App\Infrastructure\Helpers\Traits\SerializerTrait;
use App\Infrastructure\Repository\StoreRepository;
use App\Tests\AbsTest;
use Exception;

final class StoreRepositoryTest extends AbsTest {
    use SerializerTrait;

    /**
     * @test
     *
     * @throws Exception
     */
    public function haveChristmasWinterTags(): void {
        $repository = new StoreRepository();
        $filter     = (new StoreFilter())->setTagNames(['Christmas', 'Winter']);
        $expected   = [
            ['name' => 'Store 1'],
            ['name' => 'Store 2'],
            ['name' => 'Store 3'],
            ['name' => 'Store 4'],
            ['name' => 'Store 5'],
        ];
        $actual     = $repository->many($filter)->toArray();
        self::assertSame(array_diff($expected, $actual), array_diff($actual, $expected));
    }

    /**
     * @test
     *
     * @throws Exception
     */
    public function noSells(): void {
        $repository = new StoreRepository();
        $filter     = (new StoreFilter())->setHasNoSells(true);
        $expected   = [
            ['name' => 'Store 4'],
        ];
        $actual     = $repository->many($filter)->toArray();
        self::assertSame(array_diff($expected, $actual), array_diff($actual, $expected));
    }

    /**
     * @test
     *
     * @throws Exception
     */
    public function monthlyProfit(): void {
        $repository = new StoreRepository();
        $filter     = (new StoreFilter())->setShowStoreMonthly(2);
        $expected   = [
            [
                "name"                => "Store 1",
                "sales_y"             => 2019,
                "sales_m"             => 1,
                "total_profit"        => "2576",
                "total_products_sold" => 23,
            ],
            [
                "name"                => "Store 1",
                "sales_y"             => 2019,
                "sales_m"             => 2,
                "total_profit"        => "400",
                "total_products_sold" => 2,
            ],
            [
                "name"                => "Store 1",
                "sales_y"             => 2019,
                "sales_m"             => 4,
                "total_profit"        => "951",
                "total_products_sold" => 6,
            ],
            [
                "name"                => "Store 1",
                "sales_y"             => 2019,
                "sales_m"             => 7,
                "total_profit"        => "248",
                "total_products_sold" => 2,
            ],
            [
                "name"                => "Store 2",
                "sales_y"             => 2019,
                "sales_m"             => 8,
                "total_profit"        => "314",
                "total_products_sold" => 5,
            ],
        ];
        $actual     = $repository->many($filter)->toArray();
        self::assertSame(array_diff($expected, $actual), array_diff($actual, $expected));
    }
}
